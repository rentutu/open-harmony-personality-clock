# OpenHarmony__个性时钟

#### 介绍
OpenHarmony--个性时钟,  方便开发者快速调整页面效果



#### 安装教程

1.  git 地址:https://gitee.com/rentutu/open-harmony-personality-clock.git
2.  下载直接IDE运行 或者 把widget 单独下载,用引入的方式引入主页面 
3. widget是主页面  widget2 是组件页面


#### 使用说明

1. 根据文档对应使用功能 

名称类型默认值必填描述formstringloop否调整时钟样式rect 矩形默认不填 原型sizestringnomal否f调整时钟尺寸默认 normal  正常large 大small 小


2.widget是主页面  widget2 是组件页面




#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
