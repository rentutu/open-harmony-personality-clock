export default {
    data: {
        hourHand: "rotate(-90deg)",
        minuteHand: "rotate(-90deg)",
        secondHand: "rotate(-90deg)",
        scale: [
            {
                num: "1",
                style: "scale"
            },
            {
                num: "2",
                style: "scale2"
            },
            {
                num: "3",
                style: "scale3"
            },
            {
                num: "4",
                style: "scale4"
            },
            {
                num: "5",
                style: "scale5"
            },

            {
                num: "6",
                style: "scale6"
            },
            {
                num: "7",
                style: "scale7"
            },
            {
                num: "8",
                style: "scale8"
            },
            {
                num: "9",
                style: "scale9"
            },
            {
                num: "10",
                style: "scale10"
            },
            {
                num: "11",
                style: "scale11"
            },
            {
                num: "12",
                style: "scale12"
            },],
    },
    onInit() {

        const _this = this

        let hour = new Date().getHours()

        let minute = new Date().getMinutes()

        let second = new Date().getSeconds()

        hour %= 12

        _this.hourHand = "rotate(" + (hour * 30 - 90) + "deg)"

        _this.minuteHand = "rotate(" + (minute * 6 - 90) + "deg)"

        _this.secondHand = "rotate(" + (second * 6 - 90) + "deg)"

        setInterval(function () {

            let hour = new Date().getHours()

            let minute = new Date().getMinutes()

            let second = new Date().getSeconds()

            hour %= 12

            _this.hourHand = "rotate(" + (hour * 30 - 90) + "deg)"

            _this.minuteHand = "rotate(" + (minute * 6 - 90) + "deg)"

            _this.secondHand = "rotate(" + (second * 6 - 90) + "deg)"

        }, 1000);

    }
}