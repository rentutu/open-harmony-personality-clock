export default {
    props: {
        form: { //尺寸大小 rect和loop  默认
        },
        timezone: { //时差
        },
        colors: { //时钟颜色
        },
    },
    data() {
        return {
            hourHand: "rotate(-90deg)",
            minuteHand: "rotate(-90deg)",
            secondHand: "rotate(-90deg)",
            scale: [
                {
                    num: "1",
                    style: "scale"
                },
                {
                    num: "2",
                    style: "scale2"
                },
                {
                    num: "3",
                    style: "scale3"
                },
                {
                    num: "4",
                    style: "scale4"
                },
                {
                    num: "5",
                    style: "scale5"
                },

                {
                    num: "6",
                    style: "scale6"
                },
                {
                    num: "7",
                    style: "scale7"
                },
                {
                    num: "8",
                    style: "scale8"
                },
                {
                    num: "9",
                    style: "scale9"
                },
                {
                    num: "10",
                    style: "scale10"
                },
                {
                    num: "11",
                    style: "scale11"
                },
                {
                    num: "12",
                    style: "scale12"
                },],
            form: this.form,
            formStyle: "150px",
            timezone: this.timezone, //time difference
            td: Number, //time difference
            color:this.colors,
            colors:""

        }

    },
    async onReady() {
        const _this = this
        let hour = (new Date().getHours() + _this.td)
        let minute = new Date().getMinutes()
        let second = new Date().getSeconds()
        hour %= 12
        _this.hourHand = "rotate(" + (hour * 30 - 90) + "deg)"
        _this.minuteHand = "rotate(" + (minute * 6 - 90) + "deg)"
        _this.secondHand = "rotate(" + (second * 6 - 90) + "deg)"
        setInterval(function () {
            let hour = (new Date().getHours() + _this.td)
            let minute = new Date().getMinutes()
            let second = new Date().getSeconds()
            hour %= 12 //取余
            //            console.log(hour)
            _this.hourHand = "rotate(" + (hour * 30 - 90) + "deg)"
            _this.minuteHand = "rotate(" + (minute * 6 - 90) + "deg)"
            _this.secondHand = "rotate(" + (second * 6 - 90) + "deg)"
        }, 1000);
        await this.test(),
        await this.time()
        await this.color()
    },
    test() {
        var that = this
        if (that.form == "rect") {
            this.formStyle = "0px"
            console.log(that.formStyle)
        }

    },
    time() {
        var that = this
        switch (that.timezone) {
            case "USA":
                that.td = -8
                break;
            case "AUS":
                that.td = 10
                break;
            case "UK":
                that.td = 0
                break;
            case "jp":
                that.td = 9
                break;
            default:
                that.td = 8
        }
    },
    color() {
//        var that = this
        if (true) {
            console.log("2")
            this.color=this.colors
            console.log(this.color)
            console.log(typeof(this.color))
        }

    },
}